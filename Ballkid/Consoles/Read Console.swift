//
//  ViewController.swift
//  Ballkid
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class ReadConsole: NSViewController {
    
    //MARK: Properties
    
    var feed = Feed.Default.feed
    
    var recordDirectory = Default.recordDirectory
    
    //MARK: Date
    let dateFormatter = DateFormatter()
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observe()
        render()
    }
    
    //MARK: Observe
    
    func observe() {
        
        NotificationCenter.default.addObserver(forName: Feed.Notify.statusName, object: feed, queue: nil) { notification in
            DispatchQueue.main.async {
                self.renderStatus()
            }
        }
        
        NotificationCenter.default.addObserver(forName: Feed.Notify.dataName, object: feed, queue: nil) { notification in
            DispatchQueue.main.async {
                self.renderData()
                self.record()
            }
        }
        
        tick()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: Render
    
    func render() {
        titleField.stringValue = feed.title ?? ""
        urlField.stringValue = feed.url?.absoluteString ?? ""
        userField.stringValue = feed.credentials.username ?? ""
        passField.stringValue = feed.credentials.password ?? ""
        if let t = feed.refreshRate { refreshField.doubleValue = t }
        else { refreshField.stringValue = "" }
        recordButton.url = recordDirectory
        renderStatus()
        renderData()
    }
    
    func renderStatus() {
        statusLabel.stringValue = feed.status.emotion
    }
    
    func renderData() {
        
        dateFormatter.dateFormat = Default.dateFormat
        timeLabel.stringValue = dateFormatter.string(from: feed.updated)
        
        dataText.string = dataString ?? ""
    }
    
    var dataString: String? {
        guard let data = feed.data else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    var timestamp: String {
        dateFormatter.dateFormat = Default.timestamp
        return dateFormatter.string(from: feed.updated)
    }
    
    func record() {
        guard let dir = recordDirectory else { return }
        guard let data = feed.data else { return }
        var file = feed.title ?? ""
        file += timestamp
        let path = dir.appendingPathComponent(file)
        do {
            try data.write(to: path, options: .atomic)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    //MARK: Ticker
    
    var ticker = Timer()
    
    func tick() {
        ticker.invalidate()
        ticker = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            self.tock()
        }
    }
    
    func tock() {
        if feed.ticker.isValid {
            refreshButton.title = "\(Int( feed.ticker.fireDate.timeIntervalSinceNow))"
        } else {
            refreshButton.title = " "
        }
    }
    
    //MARK: Outlets
    
    @IBOutlet weak var titleField: NSTextField!
    @IBOutlet weak var urlField: NSTextField!
    @IBOutlet weak var userField: NSTextField!
    @IBOutlet weak var passField: NSTextField!
    @IBOutlet weak var refreshField: NSTextField!
    @IBOutlet weak var refreshButton: NSButton!
    @IBOutlet weak var recordButton: FileButton!
    @IBOutlet weak var timeLabel: NSTextField!
    @IBOutlet weak var statusLabel: NSTextField!
    @IBOutlet weak var dataText: NSTextView!
    
    //MARK: Actions
    
    @IBAction func changeTitle(_ sender: Any) {
        feed.title = titleField.stringValue
        Feed.Default.title = feed.title
    }
    @IBAction func changeURL(_ sender: Any) {
        feed.url = URL(string: urlField.stringValue)
        Feed.Default.url = feed.url
    }
    @IBAction func changeUser(_ sender: Any) {
        feed.credentials.username = userField.stringValue
        Feed.Default.username = feed.credentials.username
    }
    @IBAction func changePass(_ sender: Any) {
        feed.credentials.password = passField.stringValue
        Feed.Default.password = feed.credentials.password
    }
    @IBAction func changeRefresh(_ sender: Any) {
        let t = refreshField.doubleValue
        feed.refreshRate = t > 0 ? t : nil
        Feed.Default.refresh = feed.refreshRate
    }
    @IBAction func changeRecord(_ sender: Any) {
        recordDirectory = recordButton.url
        Default.recordDirectory = recordButton.url
    }
    
    @IBAction func chooseRecord(_ sender: Any) {
        recordButton.activate()
    }
    
    @IBAction func openRecordInFinder(_ sender: Any) {
        guard let url = recordDirectory else { return }
           
        NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: url.path)
    }
    
    @IBAction func pause(_ sender: Any) {
        feed.pause()
    }
    
    @IBAction func refresh(_ sender: Any) {
        feed.refresh()
    }
    
    
    struct Default {
        @Preference(key: "Record Directory")
        static var recordDirectory: URL?
        
        @Preference(key: "Date Format", def: "yyyy-MM-dd HH:mm:ss")
        static var dateFormat: String
        
        @Preference(key: "Timestamp", def: "yyyy-MM-dd_HH-mm-ss")
        static var timestamp: String
    }
    
}

