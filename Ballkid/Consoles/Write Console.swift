//
//  WriteConsole.swift
//  Ballkid
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class WriteConsole: NSViewController {
    
    
    //MARK: Properties
    
    var recordDirectory: URL? = Default.recordDirectory // source of saved data
    var destinationURL: URL? = Default.destinationURL // where to output data
    var writes: Bool = Default.writes
    var spaceTime: TimeInterval? = Default.spaceTime
    var loops: Bool = Default.loops
    var phase: Phase = .idle {
        didSet { renderPhase() }
    }
    
    var latestRecord: URL?
    var latestUpdate = Date()
    var latestData: Data? {
        didSet {
            latestUpdate = Date()
            renderData()
        }
    }
    
    //MARK: Date
    
    let dateFormatter = DateFormatter()
    
    //MARK: Life Cycle
    
    override func viewWillAppear() {
        super.viewWillAppear()
        render()
        tick()
    }
    
    override func viewDidDisappear() {
        waiter.invalidate()
    }
    
    
    //MARK: Render
    
    func render() {
        recordButton.url = recordDirectory
        destinationButton.url = destinationURL
        if let time = spaceTime {
            spaceTimeField.seconds = time
        } else {
            spaceTimeField.stringValue = ""
        }
        writesCheck.state = writes ? .on : .off
        loopsCheck.state = loops ? .on : .off
        renderData()
        renderPhase()
    }
    
    
    func renderData() {
        
        dateFormatter.dateFormat = Default.dateFormat
        timeLabel.stringValue = dateFormatter.string(from: latestUpdate)
        fileLabel.stringValue = latestRecord?.lastPathComponent ?? ""
        dataText.string = dataString ?? ""
    }
    
    func renderPhase() {
        switch phase {
            
        case .idle:     statusImage.image = #imageLiteral(resourceName: "Standby")
        case .paused:   statusImage.image = #imageLiteral(resourceName: "Pause")
        case .set:      statusImage.image = #imageLiteral(resourceName: "Standby")
        case .running:  statusImage.image = #imageLiteral(resourceName: "Continue")
        case .ended:    statusImage.image = #imageLiteral(resourceName: "End")
        }
    }
    
    var dataString: String? {
        guard let data = latestData else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    /// list of files saved in record directory
    /// sorted by modified date
    var records: [URL] {
        
        guard let directory = recordDirectory else { return [] }
        
        let fm = FileManager.default
        
        var files = [URL]()
        
        if let scan = try? fm.contentsOfDirectory(at: directory, includingPropertiesForKeys: .none, options: .skipsHiddenFiles) {
            files = scan
        }
        
        files.sort { $0.modifiedDate ?? Date() < $1.modifiedDate ?? Date() }
        
        return files
    }
    
    func record(offset: Int = 1) -> URL? {
        let records = self.records
        guard
            let latest = latestRecord,
            let index = records.firstIndex(of: latest)
            else { return records.first }
        
        let nextIndex = index + offset
        
        if records.indices.contains(nextIndex) {
            return records[nextIndex]
        } else {
            return loops ? records.first : nil
        }
        
    }
    
    func read(offset: Int = 1) {
        guard let next = record(offset: offset) else {
            blank()
            phase = .ended
            return
        }
        latestRecord = next
        do {
            let data = try Data(contentsOf: next)
            latestData = data
            write()
        } catch {
            
        }
    }
    
    func write() {
        write(to: destinationURL)
        renderData()
        wait()
    }
    
    func write(to destination: URL?) {
        guard
            writes,
            let destination = destination,
            let data = latestData
            else { return }
        do {
            try data.write(to: destination, options: .atomic)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func blank() {
        fileLabel.stringValue = ""
        dataText.string = ""
    }
    
    
    //MARK: Timer
    
    var waiter = Timer()
    
    func wait() {
        waiter.invalidate()
        guard phase == .running else { return }

        guard let nextDate = record()?.modifiedDate else {
            phase = .ended
            return
        }
        
        let latestDate = latestRecord?.modifiedDate ?? Date()
        let duration = spaceTime ?? nextDate.timeIntervalSince(latestDate)
        waiter = Timer.scheduledTimer(withTimeInterval: duration, repeats: false) { _ in
            self.next(self)
        }
    }
    
    //MARK: Ticker
    
    var ticker = Timer()
    
    func tick() {
        ticker.invalidate()
        ticker = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            self.tock()
        }
    }
    
    func tock() {
        if waiter.isValid {
            nextButton.title = "\(Int( waiter.fireDate.timeIntervalSinceNow))"
        } else {
            nextButton.title = " "
        }
    }
    
    //MARK: Outlets
    
    @IBOutlet weak var recordButton: FileButton!
    @IBOutlet weak var destinationButton: FileButton!
    @IBOutlet weak var writesCheck: NSButton!
    @IBOutlet weak var spaceTimeField: TimeField!
    @IBOutlet weak var timeLabel: NSTextField!
    @IBOutlet weak var nextButton: NSButton!
    @IBOutlet weak var fileLabel: NSTextField!
    @IBOutlet weak var dataText: NSTextView!
    @IBOutlet weak var loopsCheck: NSButton!
    @IBOutlet weak var statusImage: NSImageView!
    
    //MARK: Actions
    
    @IBAction func changeRecord(_ sender: Any) {
        recordDirectory = recordButton.url
        Default.recordDirectory = recordDirectory
    }
    
    @IBAction func changeDestination(_ sender: Any) {
        destinationURL = destinationButton.url
        Default.destinationURL = destinationURL
    }
    
    @IBAction func toggleWrites(_ sender: Any) {
        writes = writesCheck.state == .on
        Default.writes = writes
    }
    
    
    @IBAction func changeSpaceTime(_ sender: Any) {
        spaceTime = spaceTimeField.seconds
        if spaceTime == 0 { spaceTime = nil }
//        Default.spaceTime = spaceTime
    }
    
    
    @IBAction func toggleLoops(_ sender: Any) {
        loops = loopsCheck.state == .on
        Default.loops = loops
    }
    
    @IBAction func reset(_ sender: Any) {
        latestRecord = nil
        read()
        phase = .set
        waiter.invalidate()
    }
    
    @IBAction func pause(_ sender: Any) {
        phase = .paused
        waiter.invalidate()
    }
    
    @IBAction func resume(_ sender: Any) {
        phase = .running
        read()
    }
    
    @IBAction func next(_ sender: Any) {
        read()
    }
    
    @IBAction func back(_ sender: Any) {
        read(offset: -1)
    }
    
    @IBAction func chooseRecord(_ sender: Any) {
        recordButton.activate()
    }
    
    @IBAction func openRecordInFinder(_ sender: Any) {
        guard let url = recordDirectory else { return }
        
        NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: url.path)
    }
    
    @IBAction func openWriteInFinder(_ sender: Any) {
        guard let url = destinationURL else { return }
        
        NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: url.path)
    }
    
    //MARK: Phase
    
    enum Phase {
        case idle
        case paused
        case set
        case running
        case ended
    }
    
    
    //MARK: Defaults
    
    struct Default {
        @Preference(key: "Record Directory")
        static var recordDirectory: URL?
        
        @Preference(key: "Destination URL")
        static var destinationURL: URL?
        
        @Preference(key: "Date Format", def: "yyyy-MM-dd HH:mm:ss")
        static var dateFormat: String
        
        @Preference(key: "Timestamp", def: "yyyy-MM-dd_HH-mm-ss")
        static var timestamp: String
        
        @Preference(key: "SpaceTime", def: 1)
        static var spaceTime: TimeInterval?
        
        @Preference(key: "Writes", def: false)
        static var writes: Bool
        
        @Preference(key: "Loops", def: false)
        static var loops: Bool
    }
}

extension URL {
    var modifiedDate: Date? {
        let fm = FileManager.default
        guard let attributes = try? fm.attributesOfItem(atPath: self.path) else { return nil }
        return attributes[.modificationDate] as? Date
        
    }
}
