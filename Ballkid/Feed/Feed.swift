//
//  Feed.swift
//  Ballkid
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

class Feed {
    
    //MARK: Coded Properties
    var title: String?
    
    var url: URL?
    
    var credentials = Credentials()
    
    var refreshRate: TimeInterval?
    
    //MARK: Calculated properties
    
    var updated = Date()
    
    var session = URLSession()
    
    var ticker = Timer()
    
    var status = Status.none {
        didSet {
            Notify.update(status: self)
        }
    }
    
    var data: Data? {
        didSet {
            Notify.update(data: self)
        }
    }
    
    init(url: URL?) {
        self.title = url?.lastPathComponent ?? ""
        self.url = url
    }
    
    deinit {
        ticker.invalidate()
        session.invalidateAndCancel()
    }
    
    //MARK: Configure
    
    func configure() {
        session = URLSession(configuration: configuration)
    }
    
    var configuration: URLSessionConfiguration {
        let c = URLSessionConfiguration.default
        credentials.authenticate(configuration: c)
        return c
    }
    
    //MARK: Transport
    
    /// Stop refreshing
    func pause() {
        ticker.invalidate()
        status = .paused
    }
    
    /// Set refresh cycle in motion
    func tickTock() {
        ticker.invalidate()
        guard let r = refreshRate else { return }
        self.ticker = Timer.scheduledTimer(withTimeInterval: r, repeats: false) { _ in
            self.refresh()
        }
    }
    
    /// reload data from url
    func refresh() {
        configure()
        guard let u = url else { return }
        status = .fetching
        let task = session.dataTask(with: u, completionHandler:  { (data, response, error) in
            if let d = data {
                self.updated = Date()
                self.data = d
                self.status = .data
            } else {
                print(error?.localizedDescription ?? "No Data")
                self.status = .nodata
            }
        })
        task.resume()
        tickTock()
    }
    
}
