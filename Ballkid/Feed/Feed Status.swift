//
//  Feed Status.swift
//  Baseline
//
//  Created by Neal Watkins on 2020/10/21.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

extension Feed {
    
    enum Status: String {
        case none
        case fetching
        case data
        case good
        case bad
        case nodata
        case baddata
        case paused
        
        var emotion: String {
            switch self {
            case .none: return ""
            case .fetching: return "… Fetching"
            case .data: return "🍔 Got Data"
            case .good: return "👍 Good"
            case .bad: return "❗️Error"
            case .nodata: return "□ No Data"
            case .baddata: return "👎 Bad Data"
            case .paused: return "|| Paused"
            }
        }
    }
}
