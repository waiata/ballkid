//
//  Feed Credentials.swift
//  Ballkid
//
//  Created by Neal Watkins on 2020/10/21.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

extension Feed {
    
    struct Credentials: Codable {
        
        var username: String?
        var password: String?
        
        func authenticate(configuration: URLSessionConfiguration) {
            if let a = basicAuthentification {
                configuration.httpAdditionalHeaders = ["Authorization": a]
            }
        }
        
        var basicAuthentification: String? {
            
            guard let u = username, let p = password else { return nil }
            let aString = "\(u):\(p)"
            let aData = aString.data(using: .utf8)
            let credString = aData!.base64EncodedString()
            let credBasic = "Basic \(credString)"
            
            return credBasic
        }
        
    }
}
