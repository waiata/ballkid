//
//  Feed Notify.swift
//  Baseline
//
//  Created by Neal Watkins on 2020/10/21.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

extension Feed {
    
    struct Notify {
        
        static let statusName = Notification.Name("FeedStatus")
        
        static func update(status feed: Feed) {
            NotificationCenter.default.post(name: statusName, object: feed)
        }
        
        static let dataName = Notification.Name("FeedData")
        
        static func update(data feed: Feed) {
            NotificationCenter.default.post(name: dataName, object: feed)
        }
        
    }
    
}
