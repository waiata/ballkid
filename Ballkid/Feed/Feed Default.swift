//
//  Feed Default.swift
//  Ballkid
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

extension Feed {
    
    struct Default {
        
        static var feed: Feed {
            let feed = Feed(url: url)
            feed.title = title
            feed.credentials = Credentials(username: username, password: password)
            feed.refreshRate = refresh
            return feed
        }
        
        @Preference(key: "Feed Title")
        static var title: String?
        
        @Preference(key: "Feed URL")
        static var url: URL?
        
        @Preference(key: "Feed Username")
        static var username: String?
        
        @Preference(key: "Feed Password")
        static var password: String?
        
        @Preference(key: "Feed Refresh")
        static var refresh: TimeInterval?
        
    }
}
