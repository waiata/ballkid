//
//  TimeField.swift
//  Blah
//
//  Created by Neal on 2018-02-04.
//  Copyright © 2018 waiata. All rights reserved.
//

import Cocoa

@IBDesignable
class TimeField : NSTextField {
    
    @IBInspectable var format : String = "M:ss.fff"
    @IBInspectable var scrollStep : Double = 0.1
    @IBInspectable var modulate : Double = 0.0
    @IBInspectable var pinMinimum : Bool = false
    @IBInspectable var minimum : Double = 0
    @IBInspectable var pinMaximum: Bool = false
    @IBInspectable var maximum : Double = 0
    @IBInspectable var baseMinutes : Bool = false
    
    @IBInspectable var preferenceKey : String? = nil
  
    
    /// current unit
    var unit = ""
    
    /// unmodulated value
    var value : Double = 0.0 {
        didSet {
            if pinMinimum, value < minimum { value = minimum }
            if pinMaximum, value > maximum { value = maximum }
        }
    }
    
    /// modulated value
    var seconds : TimeInterval {
        get {
            if modulate == 0.0 {
                return value
            } else {
                return value - value.remainder(dividingBy: modulate)
            }
        } set {
            value = newValue
            render()
        }
    }
    
    /// formated string
    var string : String {
        get {
            return seconds.string(format: format)
        }
        set {
            read(newValue)
            render()
        }
    }
    
    override func awakeFromNib() {
        if let key = preferenceKey {
            seconds = UserDefaults.standard.double(forKey: key)
        } else {
            render()
        }
    }
    
    override func textDidChange(_ notification: Notification) {
        super.textDidChange(notification)
        read(stringValue)
    }
    
    override func textDidEndEditing(_ notification: Notification) {
        super.textDidEndEditing(notification)
        render()
        record()
    }
    
    func render() {
        self.stringValue = string
    }
    
    func record() {
        if let key = preferenceKey {
            UserDefaults.standard.set(seconds, forKey: key)
        }
    }
    
    /// parse string
    func read(_ string: String) {
        var seconds = 0.0
//        let parts = string.split(separator: ":")
        let parts = string.components(separatedBy: [":",";",",","/"])
        for part in parts {
            let d = Double(part) ?? 0.0
            seconds = seconds * 60 + d
        }
        if baseMinutes {
            if parts.count == 1 { seconds = seconds * 60 } // minutes only
        }
        value = seconds
    }
    
    override func scrollWheel(with theEvent: NSEvent) {
        let delta = Double(theEvent.scrollingDeltaY) * scrollStep
        self.value -= delta
        render()
        record()
    }
    
}
